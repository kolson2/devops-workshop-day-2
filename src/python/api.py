from typing import Dict, Any

from flask import Blueprint

api = Blueprint("api", __name__)


@api.route("/")
def home() -> Dict[str, Any]:
    return {"message": "Hello from Karen!"}
