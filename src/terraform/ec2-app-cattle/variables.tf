variable "deploy_token_username" {
  type        = string
  description = "Git username to use when cloning the repo on the app server."
}

variable "deploy_token_password" {
  type        = string
  description = "Git password to use when cloning the repo on the app server."
}

variable "ami" {
    type = string
    default = "ami-001089eb624938d9f" 
}

variable "instance_type" {
    type = string
    default = "t2.micro"
}

variable "app_port" {
    type = number
    default = 5000
}
