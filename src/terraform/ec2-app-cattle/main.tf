terraform {
  required_version = "1.2.6"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.25.0"
    }
    tls = {
      source  = "hashicorp/tls"
      version = "~> 4.0.1"
    }
  }
}

provider "aws" {
  default_tags {
    tags = {
      org     = "8thlight"
      project = "aws-devops-workshop"
      user    = "workshop-administrator"
      Name    = "karen-olson"
    }
  }
}

provider "tls" {}

locals {
  user_data = templatefile(
    "${path.module}/deploy_app_server.sh.tmpl",
    {
      deploy_token_username = var.deploy_token_username
      deploy_token_password = var.deploy_token_password
    }
  )
}

resource "aws_instance" "main" {
  ami             = var.ami
  instance_type   = var.instance_type
  key_name        = aws_key_pair.main.key_name
  security_groups = [aws_security_group.main.name]
  user_data       = base64encode(local.user_data)
}

resource "aws_key_pair" "main" {
  key_name_prefix = "aws-devops-workshop-"
  public_key      = tls_private_key.main.public_key_openssh
}

resource "tls_private_key" "main" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_default_vpc" "main" {}

resource "aws_security_group" "main" {
  vpc_id = aws_default_vpc.main.id
}

resource "aws_security_group_rule" "ssh_ingress" {
  security_group_id = aws_security_group.main.id
  type              = "ingress"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  description       = "Allow TCP ingress for SSH access."
}

resource "aws_security_group_rule" "http_ingress" {
  security_group_id = aws_security_group.main.id
  type              = "ingress"
  from_port         = var.app_port 
  to_port           = var.app_port 
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  description       = "Allow TCP ingress for HTTP access."
}

resource "aws_security_group_rule" "open_egress" {
  security_group_id = aws_security_group.main.id
  type              = "egress"
  from_port         = -1
  to_port           = -1
  protocol          = "all"
  cidr_blocks       = ["0.0.0.0/0"]
  description       = "Allow open egress on any port."
}
