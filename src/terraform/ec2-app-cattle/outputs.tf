output "hostname" {
  value = aws_instance.main.public_dns
}

output "ssh_key" {
  sensitive   = true
  value       = tls_private_key.main.private_key_pem
  description = <<EOD
The SSH private key to use when connecting to the app server.
The following command will add the SSH key to the agent.

  $ ssh-add - <<<$(terraform output -raw "ssh_key")
EOD
}