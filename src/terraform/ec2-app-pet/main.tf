terraform {
  required_version = "1.2.6"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.25.0"
    }
    tls = {
      source  = "hashicorp/tls"
      version = "~> 4.0.1"
    }
  }
}

provider "aws" {
  default_tags {
    tags = {
      org     = "8thlight"
      project = "aws-devops-workshop"
    }
  }
}

provider "tls" {}

resource "aws_instance" "main" {
  ami             = "ami-001089eb624938d9f"
  instance_type   = "t2.micro"
  key_name        = aws_key_pair.main.key_name
  security_groups = [aws_security_group.main.name]
}

resource "aws_key_pair" "main" {
  key_name_prefix = "aws-devops-workshop-"
  public_key      = tls_private_key.main.public_key_openssh
}

resource "tls_private_key" "main" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_default_vpc" "main" {}

resource "aws_security_group" "main" {
  vpc_id = aws_default_vpc.main.id
}

resource "aws_security_group_rule" "ssh_ingress" {
  security_group_id = aws_security_group.main.id
  type              = "ingress"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  description       = "Allow TCP ingress for SSH access."
}

resource "aws_security_group_rule" "http_ingress" {
  security_group_id = aws_security_group.main.id
  type              = "ingress"
  from_port         = 5000
  to_port           = 5000
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  description       = "Allow TCP ingress for HTTP access."
}

resource "aws_security_group_rule" "open_egress" {
  security_group_id = aws_security_group.main.id
  type              = "egress"
  from_port         = -1
  to_port           = -1
  protocol          = "all"
  cidr_blocks       = ["0.0.0.0/0"]
  description       = "Allow open egress on any port."
}

output "hostname" {
  value = aws_instance.main.public_dns
}

output "ssh_key" {
  sensitive   = true
  value       = tls_private_key.main.private_key_pem
  description = <<EOD
The SSH private key to use when connecting to the app server.
The following command will add the SSH key to the agent:

  $ ssh-add - <<<$(terraform output -raw "ssh_key")
EOD
}
