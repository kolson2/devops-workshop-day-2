terraform {
  required_version = "1.2.6"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.25.0"
    }
  }
  backend "local" {
    path = "terraform.tfstate"
  }
}

provider "aws" {
  default_tags {
    tags = {
      org     = "8thlight"
      project = "aws-devops-workshop"
    }
  }
}

resource "aws_s3_bucket" "main" {
  bucket = "8thlight-aws-devops-workshop-terraform-remote-state"
}
