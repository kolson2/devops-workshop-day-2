variable "iam_username" {}
variable "pgp_key" {}

resource "aws_iam_user" "main" {
  name          = var.iam_username
  force_destroy = true
}

resource "aws_iam_user_login_profile" "main" {
  user                    = aws_iam_user.main.name
  pgp_key                 = var.pgp_key
  password_reset_required = false
}

resource "aws_iam_access_key" "main" {
  user    = aws_iam_user.main.name
  pgp_key = var.pgp_key
}

output "iam_username" {
  value = aws_iam_user.main.name
}

output "encoded_iam_password" {
  value = aws_iam_user_login_profile.main.encrypted_password
}

output "access_key" {
  value = aws_iam_access_key.main.id
}

output "encrypted_secret_access_key" {
  value = aws_iam_access_key.main.encrypted_secret
}
