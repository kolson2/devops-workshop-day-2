#!/usr/bin/env bash
# Creates a new PGP key for encrypting AWS IAM user secrets.
KEYID=$1
gpg --gen-key
gpg --export "${KEYID}" | base64
