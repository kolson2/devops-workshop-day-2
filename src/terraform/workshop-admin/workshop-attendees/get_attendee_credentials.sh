#!/usr/bin/env bash
ATTENDEE_KEY=$1
AWS_ACCOUNT_ID=271487798950

IAM_USERNAME=$(terraform output -json \
    | jq -r ".attendees.value.${ATTENDEE_KEY}.iam_username")
IAM_PASSWORD=$(terraform output -json \
    | jq -r ".attendees.value.${ATTENDEE_KEY}.encoded_iam_password" \
    | base64 --decode \
    | gpg --decrypt)
AWS_ACCESS_KEY_ID=$(terraform output -json \
    | jq -r ".attendees.value.${ATTENDEE_KEY}.access_key")
AWS_SECRET_ACCESS_KEY=$(terraform output -json \
    | jq -r ".attendees.value.${ATTENDEE_KEY}.encrypted_secret_access_key" \
    | base64 --decode \
    | gpg --decrypt)

echo "Here are your credentials for AWS Account ID ${AWS_ACCOUNT_ID}:"
echo ""
echo "AWS Console"
echo "-----------"
echo "Username: ${IAM_USERNAME}"
echo "Password: ${IAM_PASSWORD}"
echo ""
echo "Programmatic Access"
echo "-------------------"
echo "export AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID}"
echo "export AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY}"
echo "export AWS_REGION=us-east-2"
