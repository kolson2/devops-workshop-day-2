terraform {
  required_version = "1.2.6"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.25.0"
    }
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "~> 3.16.1"
    }
  }
}

provider "aws" {
  default_tags {
    tags = {
      org     = "8thlight"
      project = "aws-devops-workshop"
      user    = "workshop-administrator"
    }
  }
}

provider "gitlab" {}

variable "attendees" {
  type = map(
    object({
      iam_username   = string
      gitlab_user_id = number
    })
  )
}

variable "gitlab_project_id" {}

module "attendee" {
  source   = "./modules/attendee"
  for_each = var.attendees

  iam_username = each.value.iam_username
  pgp_key      = file("public_key")
}

resource "aws_iam_group" "main" {
  name = "aws-devops-workshop-attendees"
}

resource "aws_iam_group_policy_attachment" "main" {
  group      = aws_iam_group.main.name
  policy_arn = "arn:aws:iam::aws:policy/AdministratorAccess"
}

resource "aws_iam_user_group_membership" "main" {
  for_each = module.attendee

  user   = each.value.iam_username
  groups = [aws_iam_group.main.name]
}

resource "gitlab_project_membership" "main" {
  for_each = var.attendees

  project_id   = var.gitlab_project_id
  user_id      = each.value.gitlab_user_id
  access_level = "developer"
}


output "attendees" {
  value = module.attendee
}
